<?php

namespace GqlSqlConverter\Exceptions;

use Exception;
use Throwable;

/**
 * Исключение, которое возникает при обработке значений
 */
class ConvertationException extends Exception
{
    /**
     * ConvertationException constructor.
     *
     * @param string $message
     * @param Throwable|null $previous
     */
    public function __construct($message = "", Throwable $previous = null)
    {
        parent::__construct($message, 500, $previous);
    }
}