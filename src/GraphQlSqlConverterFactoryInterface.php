<?php

namespace GqlSqlConverter;

use GqlSqlConverter\Converter\GraphQlSqlConverterInterface;

/**
 * Фабрика сервиса
 */
interface GraphQlSqlConverterFactoryInterface
{
    /**
     * Генерация сервис
     *
     * @return GraphQlSqlConverterInterface
     */
    public static function make(): GraphQlSqlConverterInterface;
}