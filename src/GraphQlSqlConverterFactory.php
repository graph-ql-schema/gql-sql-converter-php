<?php

namespace GqlSqlConverter;

use GqlRootTypeGetter\GraphQlRootTypeGetter;
use GqlSqlConverter\Converter\BoolValueProcessor;
use GqlSqlConverter\Converter\DateValueProcessor;
use GqlSqlConverter\Converter\EnumValueProcessor;
use GqlSqlConverter\Converter\FloatValueProcessor;
use GqlSqlConverter\Converter\GraphQlSqlConverter;
use GqlSqlConverter\Converter\GraphQlSqlConverterInterface;
use GqlSqlConverter\Converter\IdValueProcessor;
use GqlSqlConverter\Converter\IntValueProcessor;
use GqlSqlConverter\Converter\StringValueProcessor;

/**
 * Фабрика сервиса
 */
class GraphQlSqlConverterFactory implements GraphQlSqlConverterFactoryInterface
{
    /**
     * Генерация сервис
     *
     * @return GraphQlSqlConverterInterface
     */
    public static function make(): GraphQlSqlConverterInterface
    {
        $typeGetter = new GraphQlRootTypeGetter();
        return new GraphQlSqlConverter([
            new BoolValueProcessor($typeGetter),
            new DateValueProcessor($typeGetter),
            new EnumValueProcessor($typeGetter),
            new FloatValueProcessor($typeGetter),
            new IdValueProcessor($typeGetter),
            new IntValueProcessor($typeGetter),
            new StringValueProcessor($typeGetter),
        ]);
    }
}