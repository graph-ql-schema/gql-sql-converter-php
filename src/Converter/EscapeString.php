<?php


namespace GqlSqlConverter\Converter;

/**
 * Экранирование строки, не валидной для SQL для исключения SQL injection
 */
class EscapeString
{
    /**
     * Экранирование строки, не валидной для SQL для исключения SQL injection
     *
     * @param string $baseValue
     * @return string
     */
    public static function escape(string $baseValue): string {
        $result = str_replace("\\", "\\\\", $baseValue);
	    $replace = [
            "\x1a" => "\\Z",
            "\\0"  => "\\\\0",
            "\n"   => "\\n",
            "\r"   => "\\r",
            "'"    => "\'",
            '"'    => '\"',
        ];

	    return str_replace(array_keys($replace), array_values($replace), $result);
    }
}