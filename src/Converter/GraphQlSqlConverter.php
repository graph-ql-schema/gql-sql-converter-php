<?php

namespace GqlSqlConverter\Converter;

use GqlSqlConverter\Exceptions\ConvertationException;
use GraphQL\Type\Definition\ObjectType;

/**
 * Конвертер значений для парсера параметров
 */
class GraphQlSqlConverter implements GraphQlSqlConverterInterface
{
    /** @var GraphQlSqlConverterProcessorInterface[] */
    private $processors;

    /**
     * GraphQlSqlConverter constructor.
     *
     * @param GraphQlSqlConverterProcessorInterface[] $processors
     */
    public function __construct(array $processors)
    {
        $this->processors = $processors;
    }

    /**
     * Конвертация в базовый тип, например в строку или число
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     *
     * @return mixed
     * @throws ConvertationException
     */
    public function toBaseType(ObjectType $object, string $field, $value)
    {
        foreach ($this->processors as $processor) {
            if ($processor->isAvailable($object, $field, $value)) {
                return $processor->toBaseType($object, $field, $value);
            }
        }

        throw new ConvertationException("Can't parse field, type is not available");
    }

    /**
     * Конвертация в SQL like значение
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     *
     * @return string
     * @throws ConvertationException
     */
    public function toSQLValue(ObjectType $object, string $field, $value): string
    {
        foreach ($this->processors as $processor) {
            if ($processor->isAvailable($object, $field, $value)) {
                return $processor->toSQLValue($object, $field, $value);
            }
        }

        throw new ConvertationException("Can't parse field, type is not available");
    }
}