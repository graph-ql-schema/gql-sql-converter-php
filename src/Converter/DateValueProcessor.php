<?php

namespace GqlSqlConverter\Converter;

use DateTime;
use DateTimeInterface;
use GqlDatetime\Scalars\DateScalarType;
use GqlDatetime\Scalars\DateTimeScalarType;
use GqlDatetime\Scalars\TimeScalarType;
use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GqlSqlConverter\Exceptions\ConvertationException;
use GraphQL\Type\Definition\ObjectType;
use ReflectionObject;
use Throwable;

/**
 * Процессор значений даты/времени
 */
class DateValueProcessor extends AbstractProcessor
{
    /**
     * DateValueProcessor constructor.
     * @param GraphQlRootTypeGetterInterface $typeGetter
     */
    public function __construct(GraphQlRootTypeGetterInterface $typeGetter)
    {
        parent::__construct($typeGetter);
    }

    /**
     * Конвертация в базовый тип, например в строку или число
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     * @return mixed
     * @throws ConvertationException
     */
    public function toBaseType(ObjectType $object, string $field, $value)
    {
        /** @var DateScalarType $fieldType */
        $fieldType = $this->getFieldType($object, $field);
        $reflectionObject = new ReflectionObject($fieldType);

        try {
            $property = $reflectionObject->getProperty('format');
            $property->setAccessible(true);

            $format = $property->getValue($fieldType);
        } catch (Throwable $exception) {
            throw new ConvertationException(sprintf("Failed to parse format for field %s", $field), $exception);
        }

        switch (true) {
            case in_array(gettype($value), ["integer", "double"]):
                return (new DateTime())->setTimestamp($value);
            case gettype($value) === "string":
                $date = DateTime::createFromFormat($format, $value);
                if (false === $date) {
                    throw new ConvertationException(sprintf("You should pass correct value for field %s", $field));
                }

                return $date;
            case $value instanceof DateTimeInterface:
                return $value;
            default:
                throw new ConvertationException(sprintf("You should pass correct value for field %s", $field));
        }
    }

    /**
     * Конвертация в SQL like значение
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     * @return string
     * @throws ConvertationException
     *
     */
    public function toSQLValue(ObjectType $object, string $field, $value): string
    {
        /** @var DateTimeInterface $value */
        $value = $this->toBaseType($object, $field, $value);

        return $value->format(DATE_RFC3339_EXTENDED);
    }

    /**
     * Получение доступных для конвертации типов
     *
     * @return string[]
     */
    protected function getAvailableTypes(): array
    {
        return [DateScalarType::class, DateTimeScalarType::class, TimeScalarType::class];
    }
}