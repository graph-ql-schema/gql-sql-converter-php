<?php

namespace GqlSqlConverter\Converter;

use GqlSqlConverter\Exceptions\ConvertationException;
use GraphQL\Type\Definition\FloatType;
use GraphQL\Type\Definition\ObjectType;

/**
 * Процессор дробных значений
 */
class FloatValueProcessor extends AbstractProcessor
{
    /**
     * Получение доступных для конвертации типов
     *
     * @return string[]
     */
    protected function getAvailableTypes(): array
    {
        return [FloatType::class];
    }

    /**
     * Конвертация в базовый тип, например в строку или число
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     *
     * @return mixed
     * @throws ConvertationException
     */
    public function toBaseType(ObjectType $object, string $field, $value)
    {
        switch (gettype($value)) {
            case "NULL":
                return null;
            case "double":
            case "integer":
            case "boolean":
            case "string":
                if (strtolower($value) === "null") {
                    return null;
                }

                $val = doubleval($value);
                if (false === $val) {
                    throw new ConvertationException("Failed to parse value");
                }

                return $val;
            default:
                throw new ConvertationException("Failed to parse value");
        }
    }

    /**
     * Конвертация в SQL like значение
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     *
     * @return string
     * @throws ConvertationException
     */
    public function toSQLValue(ObjectType $object, string $field, $value): string
    {
        $value = $this->toBaseType($object, $field, $value);
        if (null === $value) {
            return 'null';
        }

        $value = EscapeString::escape($value);

        return "$value";
    }
}