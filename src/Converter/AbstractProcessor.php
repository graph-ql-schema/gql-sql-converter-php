<?php

namespace GqlSqlConverter\Converter;

use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

/**
 * Базовая реализация процессора
 */
abstract class AbstractProcessor implements GraphQlSqlConverterProcessorInterface
{
    /** @var GraphQlRootTypeGetterInterface */
    private $typeGetter;

    /**
     * AbstractProcessor constructor.
     *
     * @param GraphQlRootTypeGetterInterface $typeGetter
     */
    public function __construct(GraphQlRootTypeGetterInterface $typeGetter)
    {
        $this->typeGetter = $typeGetter;
    }

    /**
     * Получение доступных для конвертации типов
     *
     * @return string[]
     */
    abstract protected function getAvailableTypes(): array;

    /**
     * Тестирование процессора на доступность
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     *
     * @return bool
     */
    public function isAvailable(ObjectType $object, string $field, $value): bool
    {
        $fieldType = $this->getFieldType($object, $field);
        if (null === $fieldType) {
            return false;
        }
    
        foreach ($this->getAvailableTypes() as $type) {
            if($fieldType instanceof $type) {
                return true;
            }
        }
        
        return false;
    }

    /**
     * Получение базового типа для поля
     *
     * @param ObjectType $object
     * @param string $field
     * @return Type|null
     */
    protected function getFieldType(ObjectType $object, string $field): ?Type
    {
        $fieldData = ($object->getFields())[$field];
        if (!$fieldData) {
            return null;
        }

        $type = $fieldData->getType();
        return $this->typeGetter->getRootType($type);
    }
}