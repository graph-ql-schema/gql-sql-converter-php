<?php

namespace GqlSqlConverter\Converter;

use DateTimeInterface;
use GqlSqlConverter\Exceptions\ConvertationException;
use GraphQL\Type\Definition\IDType;
use GraphQL\Type\Definition\ObjectType;

/**
 * Процессор значений ID
 */
class IdValueProcessor extends AbstractProcessor
{
    /**
     * Получение доступных для конвертации типов
     *
     * @return string[]
     */
    protected function getAvailableTypes(): array
    {
        return [IDType::class];
    }

    /**
     * Конвертация в базовый тип, например в строку или число
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     *
     * @return mixed
     * @throws ConvertationException
     */
    public function toBaseType(ObjectType $object, string $field, $value)
    {
        switch (true) {
            case $value === null:
                return null;
            case in_array(gettype($value), ["integer", "double", "string"]):
                if (strtolower($value) === "null") {
                    return null;
                }

                return "$value";
            case $value instanceof DateTimeInterface:
                return $value->format(DATE_RFC3339);
            default:
                throw new ConvertationException(sprintf("Failed to parse value for field %s", $field));
        }
    }

    /**
     * Конвертация в SQL like значение
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     *
     * @return string
     * @throws ConvertationException
     */
    public function toSQLValue(ObjectType $object, string $field, $value): string
    {
        $value = $this->toBaseType($object, $field, $value);
        if (null !== $value) {
            return 'null';
        }

        $value = EscapeString::escape($value);

        return "'$value'";
    }
}