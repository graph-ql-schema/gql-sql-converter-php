<?php

namespace GqlSqlConverter\Converter;

use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GqlSqlConverter\Exceptions\ConvertationException;
use GraphQL\Type\Definition\BooleanType;
use GraphQL\Type\Definition\ObjectType;

/**
 * Процессор логических значений
 */
class BoolValueProcessor extends AbstractProcessor
{
    /**
     * BoolValueProcessor constructor.
     *
     * @param GraphQlRootTypeGetterInterface $typeGetter
     */
    public function __construct(GraphQlRootTypeGetterInterface $typeGetter)
    {
        parent::__construct($typeGetter);
    }

    /**
     * Конвертация в базовый тип, например в строку или число
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     * @return mixed
     * @throws ConvertationException
     *
     */
    public function toBaseType(ObjectType $object, string $field, $value)
    {
        switch (gettype($value)) {
            case "boolean":
                return $value;
            case "integer":
            case "double":
                return $value > 0;
            case "NULL":
                return null;
            case "string":
                if (strtolower($value) === "null") {
                    return null;
                }

                return in_array(strtolower($value), ["1", "true"]);
            default:
                throw new ConvertationException(sprintf("Failed to convert value of type %s to boolean type", gettype($value)));
        }
    }

    /**
     * Конвертация в SQL like значение
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     * @return string
     * @throws ConvertationException
     *
     */
    public function toSQLValue(ObjectType $object, string $field, $value): string
    {
        $value = $this->toBaseType($object, $field, $value);
        if (null === $value) {
            return 'null';
        }

        return $value ? 'true' : 'false';
    }

    /**
     * Получение доступных для конвертации типов
     *
     * @return string[]
     */
    protected function getAvailableTypes(): array
    {
        return [BooleanType::class];
    }
}