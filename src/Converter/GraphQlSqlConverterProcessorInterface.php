<?php

namespace GqlSqlConverter\Converter;

use GqlSqlConverter\Exceptions\ConvertationException;
use GraphQL\Type\Definition\ObjectType;

/**
 * Процессор для обработки конвертации значения
 */
interface GraphQlSqlConverterProcessorInterface
{
    /**
     * Тестирование процессора на доступность
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     *
     * @return bool
     */
    public function isAvailable(ObjectType $object, string $field, $value): bool;

    /**
     * Конвертация в базовый тип, например в строку или число
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     *
     * @return mixed
     * @throws ConvertationException
     */
    public function toBaseType(ObjectType $object, string $field, $value);

    /**
     * Конвертация в SQL like значение
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     *
     * @return string
     * @throws ConvertationException
     */
	public function toSQLValue(ObjectType $object, string $field, $value): string;
}