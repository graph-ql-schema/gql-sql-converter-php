<?php

namespace GqlSqlConverter\Converter;

use DateTimeInterface;
use GqlSqlConverter\Exceptions\ConvertationException;
use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\ObjectType;

/**
 * Обработка значений типа Enum
 */
class EnumValueProcessor extends AbstractProcessor
{
    /**
     * Получение доступных для конвертации типов
     *
     * @return string[]
     */
    protected function getAvailableTypes(): array
    {
        return [EnumType::class];
    }

    /**
     * Конвертация в базовый тип, например в строку или число
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     * @return mixed
     * @throws ConvertationException
     *
    */
    public function toBaseType(ObjectType $object, string $field, $value)
    {
        if (null === $value) {
            return null;
        }

        /** @var EnumType $fieldType */
        $fieldType = $this->getFieldType($object, $field);
        foreach ($fieldType->getValues() as $enumValue) {
            if ($enumValue->name == $value) {
                return $enumValue->value;
            }
        }

        throw new ConvertationException("Failed to get real value for passed variant");
    }

    /**
     * Конвертация в SQL like значение
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     * @return string
     * @throws ConvertationException
     *
     */
    public function toSQLValue(ObjectType $object, string $field, $value): string
    {
        $value = $this->toBaseType($object, $field, $value);
        if (null === $value) {
            return 'null';
        }

        switch (true) {
            case gettype($value) === "boolean":
                return $value ? 'true' : 'false';
            case gettype($value) === "string":
                return "'$value'";
            case in_array(gettype($value), ["integer", "double"]):
                return "$value";
            case $value instanceof DateTimeInterface:
                return $value->format(DATE_RFC3339);
            default:
                throw new ConvertationException("failed to convert value to sql");
        }
    }
}