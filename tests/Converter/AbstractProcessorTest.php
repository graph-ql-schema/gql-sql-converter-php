<?php

namespace GqlSqlConverter\Tests\Converter;

use GqlSqlConverter\Tests\Mocks\GraphQlRootTypeGetterMock;
use GraphQL\Type\Definition\IntType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\StringType;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;

/**
 * Для тестирования абстрактного класса нужна реализация, поэтому используем готовую реализацию
 */
class AbstractProcessorTest extends TestCase
{
    /**
     * Набор данных для тестирования метода isAvailable
     *
     * @return array
     */
    public function dataForIsAvailable() {
        return [
            [
                new ObjectType([
                    'name' => '1',
                    'fields' => ['test' => Type::int()],
                ]),
                [IntType::class],
                true,
            ],
            [
                new ObjectType([
                    'name' => '1',
                    'fields' => ['test' => Type::int()],
                ]),
                [StringType::class],
                false,
            ],
        ];
    }

    /**
     * Тестирование метода isAvailable
     *
     * @dataProvider dataForIsAvailable
     * @param ObjectType $object
     * @param array $availableTypes
     * @param bool $result
     */
    public function testForIsAvailable(ObjectType $object, array $availableTypes, bool $result) {
        $instance = new AbstractProcessorImplementation(
            new GraphQlRootTypeGetterMock(false, false, false),
            $availableTypes
        );

        $this->assertEquals($result, $instance->isAvailable($object, 'test', null));
    }
}