<?php

namespace GqlSqlConverter\Tests\Converter;

use DateTime;
use Exception;
use GqlDatetime\DateTimeTypes;
use GqlSqlConverter\Converter\DateValueProcessor;
use GqlSqlConverter\Exceptions\ConvertationException;
use GqlSqlConverter\Tests\Mocks\GraphQlRootTypeGetterMock;
use GraphQL\Type\Definition\ObjectType;
use PHPUnit\Framework\TestCase;
use stdClass;

class DateValueProcessorTest extends TestCase
{
    /**
     * Набор данных для тестирования валидной конвертации
     * @return array
     */
    public function dataForValidConvertation() {
        $date = DateTime::createFromFormat(DATE_RFC3339, "2020-01-01T10:00:00Z");
        return [
            ["2020-01-01T10:00:00Z", $date],
            [$date, $date],
            [$date->getTimestamp(), $date],
        ];
    }

    /**
     * Тестирование валидной конвертации
     *
     * @dataProvider dataForValidConvertation
     * @param $value
     * @param $result
     * @throws ConvertationException
     */
    public function testConvertation($value, $result) {
        $instance = new DateValueProcessor(new GraphQlRootTypeGetterMock(false, false , false));

        $this->assertEquals($result, $instance->toBaseType(
            new ObjectType([
                'name' => 'dddd',
                'fields' => [
                    'test' => DateTimeTypes::dateTime()
                ],
            ]),
            "test",
            $value
        ));
    }

    /**
     * Набор данных для тестирования не валидной конвертации
     * @return array
     */
    public function dataForInvalidConvertation() {
        return [
            [false],
            ["test"],
            [new stdClass()],
            [new Exception()],
            [[]],
            [function () {}],
        ];
    }

    /**
     * Тестирование не валидной конвертации
     *
     * @dataProvider dataForInvalidConvertation
     * @param $value
     * @expectedException \GqlSqlConverter\Exceptions\ConvertationException
     */
    public function testInvalidConvertation($value) {
        $instance = new DateValueProcessor(new GraphQlRootTypeGetterMock(false, false , false));
        $instance->toBaseType(
            new ObjectType([
                'name' => 'dddd',
                'fields' => [
                    'test' => DateTimeTypes::dateTime()
                ],
            ]),
            "test",
            $value
        );
    }
}