<?php


namespace GqlSqlConverter\Tests\Converter;

use Exception;
use GqlSqlConverter\Converter\StringValueProcessor;
use GqlSqlConverter\Exceptions\ConvertationException;
use GqlSqlConverter\Tests\Mocks\GraphQlRootTypeGetterMock;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use stdClass;

class StringValueProcessorTest extends TestCase
{
    /**
     * Набор данных для тестирования валидной конвертации
     * @return array
     */
    public function dataForValidConvertation() {
        return [
            [true, '1'],
            [false, ''],
            [1, '1'],
            [15, '15'],
            [0, '0'],
            ['true', 'true'],
            ['1', '1'],
            [null, null],
            ['Null', null],
        ];
    }

    /**
     * Тестирование валидной конвертации
     *
     * @dataProvider dataForValidConvertation
     * @param $value
     * @param $result
     * @throws ConvertationException
     */
    public function testConvertation($value, $result) {
        $instance = new StringValueProcessor(new GraphQlRootTypeGetterMock(false, false , false));

        $this->assertEquals($result, $instance->toBaseType(
            new ObjectType([
                'name' => '1',
                'fields' => ['test' => Type::int()],
            ]),
            "test",
            $value
        ));
    }

    /**
     * Набор данных для тестирования не валидной конвертации
     * @return array
     */
    public function dataForInvalidConvertation() {
        return [
            [new stdClass()],
            [new Exception()],
            [[]],
            [function () {}],
        ];
    }

    /**
     * Тестирование не валидной конвертации
     *
     * @dataProvider dataForInvalidConvertation
     * @param $value
     * @expectedException \GqlSqlConverter\Exceptions\ConvertationException
     */
    public function testInvalidConvertation($value) {
        $instance = new StringValueProcessor(new GraphQlRootTypeGetterMock(false, false , false));
        $instance->toBaseType(
            new ObjectType([
                'name' => '1',
                'fields' => ['test' => Type::int()],
            ]),
            "test",
            $value
        );
    }
}