<?php

namespace GqlSqlConverter\Tests\Converter;

use Exception;
use GqlSqlConverter\Converter\EnumValueProcessor;
use GqlSqlConverter\Exceptions\ConvertationException;
use GqlSqlConverter\Tests\Mocks\GraphQlRootTypeGetterMock;
use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\ObjectType;
use PHPUnit\Framework\TestCase;
use stdClass;

class EnumValueProcessorTest extends TestCase
{
    /**
     * Набор данных для тестирования валидной конвертации
     * @return array
     */
    public function dataForValidConvertation() {
        return [
            ['testName', 1],
        ];
    }

    /**
     * Тестирование валидной конвертации
     *
     * @dataProvider dataForValidConvertation
     * @param $value
     * @param $result
     * @throws ConvertationException
     */
    public function testConvertation($value, $result) {
        $instance = new EnumValueProcessor(new GraphQlRootTypeGetterMock(false, false , false));

        $this->assertEquals($result, $instance->toBaseType(
            new ObjectType([
                'name' => 'dddd',
                'fields' => [
                    'test' => new EnumType([
                        'name' => 'test_enum',
                        'values' => [
                            'testName' => [
                                'value' => 1,
                            ],
                        ],
                    ])
                ],
            ]),
            "test",
            $value
        ));
    }

    /**
     * Набор данных для тестирования не валидной конвертации
     * @return array
     */
    public function dataForInvalidConvertation() {
        return [
            [false],
            ["test"],
            [new stdClass()],
            [new Exception()],
            [[]],
            [function () {}],
        ];
    }

    /**
     * Тестирование не валидной конвертации
     *
     * @dataProvider dataForInvalidConvertation
     * @param $value
     * @expectedException \GqlSqlConverter\Exceptions\ConvertationException
     */
    public function testInvalidConvertation($value) {
        $instance = new EnumValueProcessor(new GraphQlRootTypeGetterMock(false, false , false));
        $instance->toBaseType(
            new ObjectType([
                'name' => 'dddd',
                'fields' => [
                    'test' => new EnumType([
                        'name' => 'test_enum',
                        'values' => [
                            'testName' => [
                                'value' => 1,
                            ],
                        ],
                    ])
                ],
            ]),
            "test",
            $value
        );
    }
}