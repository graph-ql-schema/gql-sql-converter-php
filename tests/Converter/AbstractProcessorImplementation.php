<?php

namespace GqlSqlConverter\Tests\Converter;

use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GqlSqlConverter\Converter\AbstractProcessor;
use GqlSqlConverter\Exceptions\ConvertationException;
use GraphQL\Type\Definition\ObjectType;

/**
 * Имплементация базового процессора для тестирования
 */
class AbstractProcessorImplementation extends AbstractProcessor
{
    /**
     * @var string[]
     */
    private $availableTypes;

    /**
     * AbstractProcessorImplementation constructor.
     *
     * @param GraphQlRootTypeGetterInterface $typeGetter
     * @param string[] $availableTypes
     */
    public function __construct(GraphQlRootTypeGetterInterface $typeGetter, array $availableTypes)
    {
        parent::__construct($typeGetter);
        $this->availableTypes = $availableTypes;
    }

    /**
     * Конвертация в базовый тип, например в строку или число
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     * @return mixed
     * @throws ConvertationException
     */
    public function toBaseType(ObjectType $object, string $field, $value) {
        return null;
    }

    /**
     * Конвертация в SQL like значение
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     * @return string
     * @throws ConvertationException
     *
     */
    public function toSQLValue(ObjectType $object, string $field, $value): string
    {
        return 'null';
    }

    /**
     * Получение доступных для конвертации типов
     *
     * @return string[]
     */
    protected function getAvailableTypes(): array
    {
        return $this->availableTypes;
    }
}